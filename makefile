all:
	@if [ ! -d out ]; then\
		mkdir out;\
	fi
	cp futurama out/futurama
	strfile out/futurama out/futurama.dat
	@if [ ! -L out/futurama ]; then\
		ln -s futurama out/futurama.u8;\
	fi
clean:
	@if [ -d out ]; then\
		rm -rf out;\
	fi


